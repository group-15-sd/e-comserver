# e-COMServer App

## Abstract
This project is to help businesses to connect with customers and offer online ordering, delivery, or booking services.

## Functional Specifications
To meet the users needs
Businesses can create accounts with basic information(name,contact details, location)
Busineses can manage their menus, product listings.
Businesses can set pricing for their products.
Customers can browse products by category or keywords.
Customers can place orders for pickup.
Customers can leave reviews and ratings for the businesses.
Admin can manage user accounts(business and customer)
Admin can monitor platform activities(orders)

## Technological specifications
Users will need an internet connection to access the web platform

## requirements
Django for backend
javascript for frontend
db.sqlite3 for database management
cloud hosting for web application hosting (Render Hosting)

## Group Members

Kashara Alvin Ssali       23/U/09144/PS      @KasharaAlvinSsali           alvinkashara20@gmail.com

Kabogere Salimu           23/U/0459          @salimuloma                  salimuloma@gmail.com

Nassali Catherine Laura   23/U/15495/PS      @nassali.catherine.laura     nassali.catherine.laura@gmail.com

Luganzi Mathias Joseph    23/U/24122/PS      @Mathiasjoseph               mathiasjoseph250@gmail.com

Katende Ronnie Magala     23/U/09239/PS      @trisholi                    katenderonnie045@gmail.com